package config

type Config struct {
	Database Database
	Router   Router
}
type Database struct {
	Host     string
	Port     string
	Username string
	Password string
	Db       string
}
type Router struct {
	Host string
	Port string
}
