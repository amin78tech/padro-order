package bootstrap

import (
	"padro/provider/Database"
	"padro/provider/Router"
	"padro/provider/config"
)

func Setup() {
	_ = config.Init()
	Database.Init()
	Router.Init()
}
