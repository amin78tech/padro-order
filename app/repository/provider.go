package repository

import (
	"fmt"
	"gorm.io/gorm"
	"padro/app/model"
	"padro/provider/Database"
)

type ProviderRepository struct {
	Database *gorm.DB
}

func RunConstructorProvider() *ProviderRepository {
	return &ProviderRepository{Database: Database.DatabaseInstance}
}
func (ProviderRepository *ProviderRepository) Create(data *model.Provider) (error, model.Provider) {
	res := ProviderRepository.Database.Create(data)
	if res.Error != nil {
		return fmt.Errorf("not created provider successfully"), model.Provider{}
	}
	return nil, *data
}
