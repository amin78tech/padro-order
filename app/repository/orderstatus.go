package repository

import (
	"fmt"
	"gorm.io/gorm"
	"padro/app/model"
	"padro/provider/Database"
)

type OrderStatusRepository struct {
	Database *gorm.DB
}

func RunConstructorOrderStatus() *OrderStatusRepository {
	return &OrderStatusRepository{Database: Database.DatabaseInstance}
}
func (OrderStatusRepository *OrderStatusRepository) CreateOrderStatus(data *model.OrderStatus) (error, model.OrderStatus) {
	res := OrderStatusRepository.Database.Create(data)
	if res.Error != nil {
		return fmt.Errorf("not created user successfully"), model.OrderStatus{}
	}
	return nil, *data
}
