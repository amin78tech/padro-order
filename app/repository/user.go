package repository

import (
	"fmt"
	"gorm.io/gorm"
	"padro/app/model"
	"padro/provider/Database"
)

type UserRepository struct {
	Database *gorm.DB
}

func RunConstructorUser() *UserRepository {
	return &UserRepository{Database: Database.DatabaseInstance}
}
func (UserRepository *UserRepository) Create(data *model.User) (error, model.User) {
	res := UserRepository.Database.Create(data)
	if res.Error != nil {
		return fmt.Errorf("not created user successfully"), model.User{}
	}
	return nil, *data
}
