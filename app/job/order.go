package job

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"padro/app/enum"
	"padro/app/jsonstruct"
	"padro/app/model"
	"padro/app/repository"
	"padro/app/sms"
	"padro/app/sms/gateway"
)

func UpdateStatusOrder() {
	_, orders := repository.RunConstructorOrder().GetAllOrder()
	for _, value := range orders {
		callApi(value.Provider.Api, value)
	}
}

func callApi(api string, order model.Order) {
	res, _ := http.Get(api)
	body, _ := io.ReadAll(res.Body)
	var convertData jsonstruct.ResponseApi
	json.Unmarshal(body, &convertData)
	createOrderLog(convertData, order)
}

func createOrderLog(data jsonstruct.ResponseApi, order model.Order) {
	newData := model.OrderStatus{OrderId: order.ID, Status: data.Data.Status}
	_, orderStatus := repository.RunConstructorOrderStatus().CreateOrderStatus(&newData)
	fmt.Println("Add order status", orderStatus.OrderId, orderStatus.Status)
	repository.RunConstructorOrder().UpdateOrderStatus(int(order.ID), data.Data.Status)
	if order.Status != enum.PICKED_UP && orderStatus.Status == string(enum.PICKED_UP) {
		melliSms := gateway.Melli{}
		logicSms := sms.InitSms(&melliSms)
		logicSms.SendSms("091212112", "this one step test")
	}
}
