package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"padro/app/enum"
	"padro/app/model"
	"padro/app/repository"
	"padro/app/response"
)

func CreateOrderController(context *gin.Context) {
	order := model.Order{UserId: 1, ProviderId: 1, Address: "tehran", Phone: "093652523", Status: enum.PICKED_UP}
	err, order := repository.RunConstructorOrder().Create(&order)
	if err != nil {
		context.JSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"data":    nil,
		})
	} else {
		orderResponse := response.OrderResponse{Address: order.Address, Phone: order.Phone, Status: order.Status}
		context.JSON(http.StatusOK, gin.H{
			"message": "success",
			"data":    orderResponse,
		})
	}
}
