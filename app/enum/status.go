package enum

type Status string

const (
	PICKED_UP     Status = "PICKED_UP"
	PROVIDER_SEEN Status = "PROVIDER_SEEN"
	IN_PROGRESS   Status = "IN_PROGRESS"
	PENDING       Status = "PENDING"
	DELIVERED     Status = "DELIVERED"
)
