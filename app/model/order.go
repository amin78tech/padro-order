package model

import (
	"gorm.io/gorm"
	"padro/app/enum"
)

type Order struct {
	gorm.Model
	UserId      uint   `gorm:"not null"`
	ProviderId  uint   `gorm:"not null"`
	Address     string `gorm:"not null"`
	Phone       string `gorm:"not null"`
	Provider    Provider
	Status      enum.Status
	OrderStatus OrderStatus
}
