package model

import "gorm.io/gorm"

type Provider struct {
	gorm.Model
	Name  string
	Api   string
	Order []Order
}
