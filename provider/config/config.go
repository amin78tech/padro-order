package config

import (
	"fmt"
	"github.com/spf13/viper"
	"padro/config"
)

var (
	InstanceConfig config.Config
)

func Init() config.Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error not read config file: %w", err))
	} else {
		err := viper.Unmarshal(&InstanceConfig)
		if err != nil {
			panic(fmt.Errorf("fatal error not unmarshal config file: %w", err))
		} else {
			return InstanceConfig
		}
	}
}
