package Database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"padro/provider/config"
)

var (
	DatabaseInstance *gorm.DB
)

func Init() {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		config.InstanceConfig.Database.Host,
		config.InstanceConfig.Database.Username,
		config.InstanceConfig.Database.Password,
		config.InstanceConfig.Database.Db,
		config.InstanceConfig.Database.Port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	} else {
		DatabaseInstance = db
	}
}
