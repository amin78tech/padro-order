package Route

import (
	"github.com/gin-gonic/gin"
	"padro/app/controller"
)

func OrderRouter(router *gin.Engine) {
	router.POST("/order", controller.CreateOrderController)
}
