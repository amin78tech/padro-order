package Route

import (
	"github.com/gin-gonic/gin"
	"padro/app/controller"
)

func UserRouter(router *gin.Engine) {
	router.POST("/user", controller.UserController)
}
