package Route

import (
	"github.com/gin-gonic/gin"
	"padro/app/controller"
)

func ProviderRouter(router *gin.Engine) {
	router.POST("/provider", controller.ProviderController)
}
